package game;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
        System.out.println("\"GUESS THE WORD\" GAME");
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");

        WordGame wordGame;

        wordGame = new WordGame();

        System.out.println("PLEASE ENTER LENGTH OF THE WORD");

        int length = scanner.nextInt();

        wordGame.generateWord(length);

        wordGame.printMenu();

        wordGame.chooseMenu(scanner.nextInt());



    }
}
