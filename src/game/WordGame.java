package game;

import java.util.InputMismatchException;
import java.util.Scanner;

class WordGame {
    private String word;
    private Scanner scanner = new Scanner(System.in);


    void generateWord(int length) {
       try {
           if (length > 0 && length < 11) {
               switch (length) {
                   case 1:
                       word = "x";
                       break;
                   case 2:
                       word = "op";
                       break;
                   case 3:
                       word = "pis";
                       break;
                   case 4:
                       word = "ussr";
                       break;
                   case 5:
                       word = "dunaj";
                       break;
                   case 6:
                       word = "kajaak";
                       break;
                   case 7:
                       word = "costams";
                       break;
                   case 8:
                       word = "bleblebl";
                       break;
                   case 9:
                       word = "dziewiec9";
                       break;
                   case 10:
                       word = "dziesiec10";
                       break;
                   case 11:
                       word = "jedenascie1";
                       break;
               }
           } else {
               System.out.println("PLEASE ENTER A NUMBER IN A 1-11 RANGE");
               generateWord(scanner.nextInt());
           }
       }catch (InputMismatchException x) {
           System.out.println("PLEASE ENTER A NUMBER");
           generateWord(scanner.nextInt());
       }
    }

    private int getWordLength() {
        return getWord().length();
    }

    private String getWord() {
        return word;
    }

    private String getHint(String input) {
        StringBuilder stringBuilder = new StringBuilder();
        if (input.length() == getWordLength()) {
            char[] wordArray = getWord().toCharArray();
            char[] inputArray = input.toCharArray();
            for (int i = 0; i < getWordLength(); i++) {
                if (wordArray[i] == inputArray[i]) {
                    stringBuilder.append("x");
                } else {
                    stringBuilder.append("-");
                }
            }
        } else {
            return "ENTER A " + getWordLength() + " LETTER WORD";
        }
        return stringBuilder.toString();
    }

    private String guessWord(String input) {
        if (input.equalsIgnoreCase(getWord()))
            return "CONGRATULATIONS! " + input.toUpperCase() + " IS A CORRECT WORD";
        else
            return "TRY AGAIN!";
    }

    void printMenu() {
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
        System.out.println("WHAT DO YOU WANT TO DO?");
        System.out.println("1. HINT");
        System.out.println("2. GUESS");
        System.out.println("3. NEW WORD");
        System.out.println("4. EXIT");
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
    }

    void chooseMenu(int choose) {
        try {
            if(choose > 0 && choose < 5) {
                switch (choose) {
                    case 1:
                        System.out.println("<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>");
                        System.out.println("ENTER A " + getWordLength() + " LETTER WORD TO GET A HINT:");
                        System.out.println(getHint(scanner.next()));
                        printMenu();
                        choose = scanner.nextInt();
                        chooseMenu(choose);
                        break;
                    case 2:
                        System.out.println("TRY TO GUESS THE WORD!");
                        System.out.println(guessWord(scanner.next()));
                        printMenu();
                        choose = scanner.nextInt();
                        chooseMenu(choose);
                        break;
                    case 3:
                        System.out.println("PLEASE ENTER LENGTH OF NEW WORD");
                        generateWord(scanner.nextInt());
                        printMenu();
                        choose = scanner.nextInt();
                        chooseMenu(choose);
                        break;
                    case 4:
                        System.out.println("BYE BYE!");
                        break;
                }
            } else {
                System.out.println("PLEASE ENTER A NUMBER IN A 1-4 RANGE!");
                printMenu();
                choose = scanner.nextInt();
                chooseMenu(choose);
            }
        }   catch (InputMismatchException x) {
            System.out.println("PLEASE ENTER A NUMBER");
            printMenu();
            choose = scanner.nextInt();
            chooseMenu(choose);
        }
    }


}
